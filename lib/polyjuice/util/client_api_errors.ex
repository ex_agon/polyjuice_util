# Copyright 2021 Nicolas Jouanin <nico@beerfactory.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-FileCopyrightText: 2021 Nicolas Jouanin <nico@beerfactory.org>
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Util.ClientAPIErrors do
  import Polyjuice.Util.ClientError
  alias Polyjuice.Util.ClientAPIErrors

  # Errors defined in https://matrix.org/docs/spec/client_server/r0.6.1#api-standards

  # Common error codes
  deferror(ClientAPIErrors.MForbidden, "You are not allowed to access this")
  deferror(ClientAPIErrors.MUnknownToken, "This access token is not valid")
  deferror(ClientAPIErrors.MMissingToken, "No access token provided")
  deferror(ClientAPIErrors.MBadJson, "The request was malformed")
  deferror(ClientAPIErrors.MNotJson, "The request was not valid JSON")
  deferror(ClientAPIErrors.MNotFound, "Not found")
  deferror(ClientAPIErrors.MLimitExceeded, "Limit exceeded")
  deferror(ClientAPIErrors.MUnknown, "Unknown error")

  # Other error codes
  deferror(ClientAPIErrors.MUnrecognized, "Unrecognized request")
  deferror(ClientAPIErrors.MUnauthorized, "The request was unauthorized")
  deferror(ClientAPIErrors.MUserDeactivated, "Your account is deactivated")
  deferror(ClientAPIErrors.MUserInUse, "The user ID is already in use")
  deferror(ClientAPIErrors.MInvalidUsername, "The user ID is invalid")
  deferror(ClientAPIErrors.MRoomInUse, "The room alias is already in use")
  deferror(ClientAPIErrors.MInvalidRoomState, "The room state is invalid")
  deferror(ClientAPIErrors.MThreepidInUse, "The third-party ID is in use")
  deferror(ClientAPIErrors.MThreepidNotFound, "The third-party ID could not be found")
  deferror(ClientAPIErrors.MThreepidAuthFailed, "Third-party ID authentication failed")
  deferror(ClientAPIErrors.MThreepidDenied, "The third-party ID is not allowed")
  deferror(ClientAPIErrors.MServerNotTrusted, "The requested server is not trusted")
  deferror(ClientAPIErrors.MUnsupportedRoomVersion, "The requested room version is not supported")

  deferror(
    ClientAPIErrors.MIncompatibleRoomVersion,
    "The room uses a version that is not supported"
  )

  deferror(ClientAPIErrors.MBadState, "The state change could not be performed")
  deferror(ClientAPIErrors.MGuestAccessForbidden, "Guest access is not allowed")
  deferror(ClientAPIErrors.MCaptchaNeeded, "A CAPTCHA must be completed")
  deferror(ClientAPIErrors.MCaptchaInvalid, "The CAPTCHA was incorrect")
  deferror(ClientAPIErrors.MMissingParam, "A parameter is missing")
  deferror(ClientAPIErrors.MInvalidParam, "A parameter is invalid")
  deferror(ClientAPIErrors.MTooLarge, "The request or entity is too large")
  deferror(ClientAPIErrors.MExclusive, "The resource is reserved by an application service")
  deferror(ClientAPIErrors.MResourceLimitExceeded, "A resource limit has been exceeded")

  deferror(
    ClientAPIErrors.MCannotLeaveServerNoticeRoom,
    "You cannot leave the server notice room"
  )

  deferror(
    ClientAPIErrors.MWrongRoomKeysVersion,
    "Wrong backup version"
  )
end
