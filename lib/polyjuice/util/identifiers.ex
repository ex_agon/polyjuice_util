# Copyright 2021 Nicolas Jouanin <nico@beerfactory.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-FileCopyrightText: 2021 Nicolas Jouanin <nico@beerfactory.org>
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Util.Identifiers do
  @callback new!(term) :: term
  @callback new(term) :: {:ok, term} | {:error, atom}
  @callback parse(String.t()) :: {:ok, term} | {:error, atom}
  @callback valid?(term) :: true | false
  @callback valid?(String.t()) :: true | false
end
