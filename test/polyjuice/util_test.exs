# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-FileCopyrightText: 2019 Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.UtilTest do
  use ExUnit.Case
  doctest Polyjuice.Util

  test "escape localpart" do
    import Polyjuice.Util, only: [escape_localpart: 2, escape_localpart: 1]
    assert escape_localpart("a") == "a"
    assert escape_localpart("a_") == "a__"
    assert escape_localpart("a_", fold_case: true) == "a_"
  end
end
